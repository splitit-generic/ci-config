from slack_sdk.webhook import WebhookClient
import os
import sys


def main():
	try:
			
		url = "https://hooks.slack.com/services/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"
		webhook = WebhookClient(url)
		message = "\nProject: " +  os.getenv("CI_PROJECT_NAME")
		message += "\nBranch/Tag: " +  os.getenv("CI_COMMIT_REF_NAME")
		message += "\nStatus: " +  os.getenv("CI_JOB_STATUS")
		if (os.getenv("CI_JOB_STATUS") == "success"):
			message += "\nArtifact URL: " +  os.getenv("CI_JOB_URL") + "/artifacts/download?file_type=sast"

		response = webhook.send(
			text= "Job " + os.getenv("CI_JOB_NAME") + " completed",
			blocks=
			[
				{
					"type": "header",
					"text": 
					{
						"type": "plain_text",
						"text": "Job " + os.getenv("CI_JOB_NAME") + " completed"
					}
				},
				{
					"type": "section",
					"text": 
					{
						"type": "mrkdwn",
						"text": message
					}
				}
			]
		)
		assert response.status_code == 200
		assert response.body == "ok"


		
	except Exception as ex:
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)
		print( "Error: " + str(ex))
		return 1 # 
	else:
		return 0 # 

# this is the standard boilerplate that calls the main()	 function
if __name__ == '__main__':
    # sys.exit(main(sys.argv)) # used to give a better look to exists
    main()
