import os
import sys
import json


def main():
	try:
		result_dict = {
			'project': os.getenv("CI_PROJECT_NAME"),
			'commit_ref': os.getenv("CI_COMMIT_REF_NAME"),
			'job_status': os.getenv("CI_JOB_STATUS")
				}

		if (os.getenv("CI_JOB_STATUS") == "success"):
			# Read  the artifact file
			artif_file = open("gl-sast-report.json", "r")
			artif_object = json.load(artif_file)
			result_dict['vulnerabilities'] = artif_object['vulnerabilities']
			result_dict['artifact_url'] = os.getenv("CI_JOB_URL") + "/artifacts/download?file_type=sast"

		with open(os.getenv("CI_COMMIT_SHA") + '.json', 'w') as json_file:
			json.dump(result_dict, json_file)
		print ("Created JSON file " + os.getenv("CI_COMMIT_SHA") + '.json')
	
	except Exception as ex:
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		print(exc_type, fname, exc_tb.tb_lineno)
		print( "Error: " + str(ex))
		return 1 # 
	else:
		return 0 # 

# this is the standard boilerplate that calls the main()	 function
if __name__ == '__main__':
    # sys.exit(main(sys.argv)) # used to give a better look to exists
    main()
